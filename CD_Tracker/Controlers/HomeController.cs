﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using CD_Tracker.Models;
namespace CD_Tracker.Controlers
{
    /*
     1. Access Cd model propery values of a single instance of CD
     2. Calculate maturity date from Term length and start date
     3. assign maturity date to CD.Finish
     4. calculate maturity value from term length, rate and initial deposit
     5. assign mature value to CD.MatureValue
     6. add current cd to portfolio
     7. display cd in list cds view
     8. display list cds view in index
     9. validation
     10. ui
     */
    public class HomeController : Controller
    {
        public static List<string> cds { get; set; }
        public ActionResult Index()
        {
            string noCd = "";
         
          

            ViewBag.h = noCd;

            return View();
        }
        [HttpGet]
        public ActionResult AddCD()
        {
            return View();
        }

        [HttpPost]
        public ActionResult AddCD(CD cd)
        {
            if (ModelState.IsValid)
            {
                //
                cd.Calculate();
                Portfolio.AddCd(cd);
                return View("ListCds", Portfolio.CdsAdded);
            }
            else
            {
                return View();
            }
        }

        public ActionResult ListCds()
        {
            return View(Portfolio.CdsAdded);
        }
    }
}
