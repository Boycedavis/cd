﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CD_Tracker.Models
{
    public class Portfolio
    {
        private static List<CD> cdsAdded;
        public static List<CD> CdsAdded {
            get { return cdsAdded == null ? new List<CD>() : cdsAdded; }
            private set {
                cdsAdded = value;
            }
        }

        public static void AddCd(CD cd)
        {
            if (cdsAdded == null)
            {
                cdsAdded = new List<CD>();
            }
            cdsAdded.Add(cd);
        }
    }
}
